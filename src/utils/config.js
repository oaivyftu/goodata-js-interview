import GRAPH_CONSTS from './constants';

export const generateDate = month => ({
  start: ["2016", ("0" + month).slice(-2), new Date(2016, month - 1, 1).getDate()].join('-'),
  end: ["2016", ("0" + month).slice(-2), new Date(2016, month, 0).getDate()].join('-'),
});

export const getMonthFilter = month => [{
  absoluteDateFilter: {
    dataSet: {
      uri: GRAPH_CONSTS.GRAPH_DATE_ATTRIBUTE_BASE
    },
    from: generateDate(month).start,
    to: generateDate(month).end
  }
}];
